package com.geeksforgeeks.problem;

import java.util.Scanner;

public class SaveIronMan {
	public static void main(String ... args) {
		Scanner in = new Scanner(System.in);
		String testCaseStr = in.nextLine();
		int testCase = Integer.parseInt(testCaseStr);
		StringBuffer str = null;
		boolean flag = true;
		int length;
		
		while(testCase-- > 0) {
			str = new StringBuffer(in.nextLine());
			
			for(int i=0;i<str.length();i++) {
				int value = (int) str.charAt(i);
				if(!((value>=65 && value<=90) || (value>=97 && value<=122) || (value>=48 && value<=57))) {
					str.deleteCharAt(i);
					i--;
				}
			}
			
			flag = true;
			length = str.length();
			for(int i=0;i<length/2;i++) {
				if(Character.toLowerCase(str.charAt(i)) != Character.toLowerCase(str.charAt(length-(i+1)))) {
					flag = false;
					break;
				}
			}
			if(flag)
				System.out.println("YES");
			else
				System.out.println("NO");
		}
		
	}
}